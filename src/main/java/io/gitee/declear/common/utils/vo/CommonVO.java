package io.gitee.declear.common.utils.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * common vo
 * @author dec
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonVO {

    private String id;
    private String name;

}
