package io.gitee.declear.common.utils;

import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * http utils
 * @author dec
 */
@Slf4j
public class HttpUtils {

    private static final int CONNECT_TIME_OUT = 10000;

    private static final int REQUEST_TIME_OUT = 10000;

    private static final String CHARSET = StandardCharsets.UTF_8.name();

    /**
     * http get 请求
     * @param url
     * @param parameters
     * @return
     */
    public static String get(String url, Map<String, String> headers, Map<String, String> parameters) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setHeaders(map2Array4Header(headers));
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpGet.setConfig(requestConfig);

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();
            CloseableHttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("get url: {}, parameters: {}", url, JSONObject.toJSONString(parameters), ex);
            return "";
        }
    }

    /**
     * http get 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param parameters
     * @return
     */
    public static String getWithCredentials(String name, String password, String url, Map<String, String> headers, Map<String, String> parameters) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setHeaders(map2Array4Header(headers));
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpGet.setConfig(requestConfig);

        try {
            HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            UsernamePasswordCredentials npCredential = new UsernamePasswordCredentials(name, password);
            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), npCredential);
            AuthCache authCache = new BasicAuthCache();
            authCache.put(host, new BasicScheme());
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);
            CloseableHttpResponse response = httpClient.execute(host, httpGet, localContext);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("get url: {}, parameters: {}", url, JSONObject.toJSONString(parameters), ex);
            return "";
        }
    }

    /**
     * http post 请求
     * @param url
     * @param content
     * @return
     */
    public static String post(String url, Map<String, String> headers, String content) {
        return post(url,headers, null, content);
    }

    /**
     * http post 请求
     * @param url
     * @param parameters
     * @param content
     * @return
     */
    public static String post(String url, Map<String, String> headers, Map<String, String> parameters, String content) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpPost httpPost = new HttpPost(uri);
        httpPost.setHeaders(map2Array4Header(headers));
        StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
        httpPost.setEntity(entity);
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpPost.setConfig(requestConfig);

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();
            CloseableHttpResponse response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error("post url:{}, parameters: {}, post body: {}, statusCode: {}", url, JSONObject.toJSONString(parameters),
                        JSONObject.toJSONString(content), statusCode);
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("post url:{}, parameters: {}, post body: {}", url, JSONObject.toJSONString(parameters), JSONObject.toJSONString(content), ex);
            return "";
        }
    }

    /**
     * http post 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param content
     * @return
     */
    public static String postWithCredentials(String name, String password, String url, Map<String, String> headers, String content) {
        return postWithCredentials(name, password, url, headers, null, content);
    }

    /**
     * http post 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param parameters
     * @param content
     * @return
     */
    public static String postWithCredentials(String name, String password, String url, Map<String, String> headers, Map<String, String> parameters, String content) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpPost httpPost = new HttpPost(uri);
        httpPost.setHeaders(map2Array4Header(headers));
        StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
        httpPost.setEntity(entity);
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpPost.setConfig(requestConfig);

        try {
            HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            UsernamePasswordCredentials npCredential = new UsernamePasswordCredentials(name, password);
            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), npCredential);
            AuthCache authCache = new BasicAuthCache();
            authCache.put(host, new BasicScheme());
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);
            CloseableHttpResponse response = httpClient.execute(host, httpPost, localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error("post url:{}, parameters: {}, post body: {}, statusCode: {}", url, JSONObject.toJSONString(parameters),
                        JSONObject.toJSONString(content), statusCode);
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("post url:{}, parameters: {}, post body: {}", url, JSONObject.toJSONString(parameters), JSONObject.toJSONString(content), ex);
            return "";
        }
    }

    /**
     * http put 请求
     * @param url
     * @param headers
     * @param content
     * @return
     */
    public static String put(String url, Map<String, String> headers, String content) {
        return put(url, headers, null, content);
    }

    /**
     * http put 请求
     * @param url
     * @param parameters
     * @param content
     * @return
     */
    public static String put(String url, Map<String, String> headers, Map<String, String> parameters, String content) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpPut httpPut = new HttpPut(uri);
        httpPut.setHeaders(map2Array4Header(headers));
        StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
        httpPut.setEntity(entity);
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpPut.setConfig(requestConfig);

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();
            CloseableHttpResponse response = httpClient.execute(httpPut);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error("put url:{}, parameters: {}, post body: {}, statusCode: {}", url, JSONObject.toJSONString(parameters),
                        JSONObject.toJSONString(content), statusCode);
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("put url:{}, parameters: {}, post body: {}", url, JSONObject.toJSONString(parameters), JSONObject.toJSONString(content), ex);
            return "";
        }
    }

    /**
     * http put 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param content
     * @return
     */
    public static String putWithCredentials(String name, String password, String url, Map<String, String> headers, String content) {
        return putWithCredentials(name, password, url, headers, null, content);
    }

    /**
     * http put 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param parameters
     * @param content
     * @return
     */
    public static String putWithCredentials(String name, String password, String url, Map<String, String> headers, Map<String, String> parameters, String content) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpPut httpPut = new HttpPut(uri);
        httpPut.setHeaders(map2Array4Header(headers));
        StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
        httpPut.setEntity(entity);
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpPut.setConfig(requestConfig);

        try {
            HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            UsernamePasswordCredentials npCredential = new UsernamePasswordCredentials(name, password);
            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), npCredential);
            AuthCache authCache = new BasicAuthCache();
            authCache.put(host, new BasicScheme());
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);
            CloseableHttpResponse response = httpClient.execute(host, httpPut, localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error("put url:{}, parameters: {}, post body: {}, statusCode: {}", url, JSONObject.toJSONString(parameters),
                        JSONObject.toJSONString(content), statusCode);
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("put url:{}, parameters: {}, post body: {}", url, JSONObject.toJSONString(parameters), JSONObject.toJSONString(content), ex);
            return "";
        }
    }

    /**
     * http delete 请求
     * @param url
     * @param parameters
     * @return
     */
    public static String delete(String url, Map<String, String> headers, Map<String, String> parameters) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpDelete httpDelete = new HttpDelete(uri);
        httpDelete.setHeaders(map2Array4Header(headers));
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpDelete.setConfig(requestConfig);

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();
            CloseableHttpResponse response = httpClient.execute(httpDelete);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("delete url: {}, parameters: {}", url, JSONObject.toJSONString(parameters), ex);
            return "";
        }
    }

    /**
     * http delete 请求(使用用户名/密码验证)
     * @param name
     * @param password
     * @param url
     * @param headers
     * @param parameters
     * @return
     */
    public static String deleteWithCredentials(String name, String password, String url, Map<String, String> headers, Map<String, String> parameters) {
        URI uri = getUri(url, parameters);
        if (uri == null) {
            return "";
        }

        HttpDelete httpDelete = new HttpDelete(uri);
        httpDelete.setHeaders(map2Array4Header(headers));
        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectTimeout(CONNECT_TIME_OUT);
        builder.setConnectionRequestTimeout(REQUEST_TIME_OUT);

        RequestConfig requestConfig = builder.build();
        httpDelete.setConfig(requestConfig);

        try {
            HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            UsernamePasswordCredentials npCredential = new UsernamePasswordCredentials(name, password);
            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), npCredential);
            AuthCache authCache = new BasicAuthCache();
            authCache.put(host, new BasicScheme());
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);
            CloseableHttpResponse response = httpClient.execute(host, httpDelete, localContext);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(response.getEntity(), CHARSET);
            } else {
                log.error(EntityUtils.toString(response.getEntity(), CHARSET));
                return "";
            }
        } catch (IOException ex) {
            log.error("delete url: {}, parameters: {}", url, JSONObject.toJSONString(parameters), ex);
            return "";
        }
    }

    /**
     * 生成URI对象
     * @param url
     * @param parameters
     * @return
     */
    public static URI getUri(String url, Map<String, String> parameters) {
        try {
            URIBuilder uriBuilder = new URIBuilder(url.trim());
            if (CommonUtils.isEmpty(parameters)) {
                return uriBuilder.build();
            } else {
                List<NameValuePair> list = map2List4Parameter(parameters);
                return uriBuilder.setParameters(list).build();
            }
        } catch (URISyntaxException ex) {
            log.error("getUri url: {}, parameters: {}", url, JSONObject.toJSONString(parameters), ex);
            return null;
        }
    }

    private static List<NameValuePair> map2List4Parameter(Map<String, String> parameters) {
        if(CommonUtils.isNotEmpty(parameters)) {
            List<NameValuePair> list = new ArrayList<>(parameters.size());
            parameters.forEach((key, value) -> list.add(new BasicNameValuePair(key, value)));
            return list;
        }
        return new ArrayList<>();
    }

    private static Header[] map2Array4Header(Map<String, String> headerMap) {
        if(CommonUtils.isNotEmpty(headerMap)) {
            Header[] headers = new Header[headerMap.size()];
            headerMap.forEach((key, value) -> Arrays.fill(headers, new BasicHeader(key, value)));

            return headers;
        }
        return null;
    }
}
