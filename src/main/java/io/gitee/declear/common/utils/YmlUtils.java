package io.gitee.declear.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.reader.UnicodeReader;

import java.io.IOException;
import java.io.Reader;
import java.util.*;

/**
 * yml utils
 * @author DEC
 */
@Slf4j
public class YmlUtils {

    private static final String CLASS_PATH_CURRENT_PATTERN = "classpath:";

    private static final String YML_LIST_START_CHARACTER = "[";

    /**
     * 从项目的resources目录中读取yml文件中key配置参数
     * @param filePath
     * @param key
     * @return
     */
    public static String getProperty(String filePath, String key) throws IOException {
        return getProperty(filePath, key, null);
    }

    /**
     * 从项目的resources目录中读取yml文件中key配置参数
     * @param filePath
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getProperty(String filePath, String key, String defaultValue) throws IOException {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(CLASS_PATH_CURRENT_PATTERN + filePath);
        if(resource.exists()) {
            return analysePropertyFromYml(resource, key);
        }
        log.error("can not find file: {}", filePath);
        return defaultValue;
    }

    private static String analysePropertyFromYml(Resource resource, String name) {
        int count = 0;
        try {
            if (log.isDebugEnabled()) {
                log.debug("Loading from YAML: {}", resource);
            }
            try (Reader reader = new UnicodeReader(resource.getInputStream())) {
                for (Object object : createYaml().loadAll(reader)) {
                    if (object != null) {
                        Map<String, String> result = new HashMap<>(2 << 5);
                        buildFlattenedMap(result, asMap(object), null);
                        if(CommonUtils.isNotEmpty(result)) {
                            return result.get(name);
                        }
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug("Loaded {} document/documents from YAML resource: {}", count, resource);
                }
            }
        } catch (IOException ex) {
            log.error("Load from YAML: {} error.", resource, ex);
        }

        return null;
    }

    private static void buildFlattenedMap(Map<String, String> result, Map<?, ?> source, @Nullable String path) {
        source.forEach((key, value) -> {
            String tempKey = key.toString();
            if (StringUtils.hasText(path)) {
                if (key.toString().startsWith(YML_LIST_START_CHARACTER)) {
                    tempKey = path + key;
                } else {
                    tempKey = path + '.' + key;
                }
            }

            if (value instanceof Map<?, ?>) {
                Map<?, ?> map = (Map<?, ?>) value;
                buildFlattenedMap(result, map, tempKey);
            } else if (value instanceof Collection) {
                Collection<?> collection = (Collection<?>) value;
                if (collection.isEmpty()) {
                    result.put(tempKey, "");
                }
                else {
                    int count = 0;
                    for (Object object : collection) {
                        buildFlattenedMap(result, Collections.singletonMap(
                                "[" + (count++) + "]", object), tempKey);
                    }
                }
            } else {
                result.put(tempKey, value != null ? value.toString() : null);
            }
        });
    }

    private static Map<String, Object> asMap(Object object) {
        Map<String, Object> result = new LinkedHashMap<>();
        if (!(object instanceof Map)) {
            result.put("document", object);
            return result;
        }

        @SuppressWarnings("unchecked")
        Map<Object, Object> map = (Map<Object, Object>) object;
        map.forEach((key, value) -> {
            if (value instanceof Map) {
                value = asMap(value);
            }
            if (key instanceof CharSequence) {
                result.put(key.toString(), value);
            }
            else {
                result.put("[" + key.toString() + "]", value);
            }
        });
        return result;
    }

    private static Yaml createYaml() {
        LoaderOptions loaderOptions = new LoaderOptions();
        loaderOptions.setAllowDuplicateKeys(false);

        return new Yaml(loaderOptions);
    }

    /**
     * 从项目的resources目录中读取yml文件配置参数
     * @param filePath
     * @return
     */
    public static Map<String, String> getProperties(String filePath) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(CLASS_PATH_CURRENT_PATTERN + filePath);
        if(resource.exists()) {
            return analysePropertyFromYml(resource);
        }
        log.error("can not find file: {}", filePath);
        return null;
    }

    private static Map<String, String> analysePropertyFromYml(Resource resource) {
        Map<String, String> result = new HashMap<>(2 << 5);
        int count = 0;
        try {
            if (log.isDebugEnabled()) {
                log.debug("Loading from YAML: {}", resource);
            }
            try (Reader reader = new UnicodeReader(resource.getInputStream())) {
                for (Object object : createYaml().loadAll(reader)) {
                    if (object != null) {
                        buildFlattenedMap(result, asMap(object), null);
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug("Loaded {} document/documents from YAML resource: {}", count, resource);
                }
            }
        } catch (IOException ex) {
            log.error("Load from YAML: {} error.", resource, ex);
        }

        return result;
    }

}
