package io.gitee.declear.common.utils;

import org.springframework.util.DigestUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * md5 utils
 * @author DEC
 */
public class Md5Utils {

    /**
     * 使用 java.util.Map 中的 key 生成 MD5 String
     * @param map
     * @return
     * @param <T>
     * @throws IOException
     */
    public static <T> String generateMapMd5(Map<String, T> map) throws IOException {
        if(CommonUtils.isNotEmpty(map)) {
            List<String> keyList =new ArrayList<>();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            map.forEach((key, value) -> keyList.add(key));
            Collections.sort(keyList);
            for(String key : keyList) {
                oos.writeObject(key);
            }

            return DigestUtils.md5DigestAsHex(bos.toByteArray());
        }
        return null;
    }

    /**
     * 使用 java.util.Map 中的 key 和 value 生成 MD5 String
     * @param map
     * @return
     * @param <T>
     * @throws IOException
     */
    public static <T> String generateMapMd5WithValues(Map<String, T> map) throws IOException {
        if(CommonUtils.isNotEmpty(map)) {
            List<String> keyList =new ArrayList<>();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            map.forEach((key, value) -> keyList.add(key));
            Collections.sort(keyList);
            for(String key : keyList) {
                oos.writeObject(key);
                oos.writeObject(map.get(key).toString());
            }

            return DigestUtils.md5DigestAsHex(bos.toByteArray());
        }
        return null;
    }

}
