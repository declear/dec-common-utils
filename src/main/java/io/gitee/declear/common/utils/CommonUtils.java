package io.gitee.declear.common.utils;

import io.gitee.declear.common.utils.enums.ResponseCode;
import io.gitee.declear.common.utils.vo.ResultVO;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 通用工具类
 * @author dec
 */
public class CommonUtils {

    /**
     * 网卡名称： docker虚拟网卡名称
     */
    private static final String NETWORK_INTERFACE_NAME_DOCKER = "docker";


    /**
     * common java.time.LocalDateTime formatter
     */
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * common java.time.LocalDate formatter
     */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * 查找参数名的工具类
     */
    private static final LocalVariableTableParameterNameDiscoverer DISCOVERER = new LocalVariableTableParameterNameDiscoverer();

    /**
     * 本机ip地址
     */
    private static InetAddress localhostIPAddress = null;

    /**
     * check object is null or not
     * @param object
     * @return
     */
    public static boolean isEmpty(Object object) {
        if(object == null) {
            return true;
        } else if(object instanceof String) {
            return ((String) object).trim().isEmpty();
        } else if(object instanceof Collection) {
            return ((Collection<?>) object).isEmpty();
        } else if(object instanceof Map) {
            return ((Map<?, ?>) object).isEmpty();
        } else if(object instanceof Object[]) {
            return ((Object[]) object).length == 0;
        } else {
            return false;
        }
    }

    /**
     * check object is null or not
     * @param object
     * @return
     */
    public static boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }

    /**
     * generate UUID string without '-'
     * @return
     */
    public static String UUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 判断相等
     * @param a
     * @param b
     * @return
     */
    public static boolean isEquals(Object a, Object b) {
        if(a != null && b != null) {
            return a.equals(b);
        }
        return false;
    }

    /**
     * 返回API调用结果统一封装对象: success
     * @param message
     * @return
     */
    public static ResultVO resultSuccess(String message) {
        ResultVO result = new ResultVO();
        result.setTotal(0L);
        result.setMessage(message);
        result.setCode(ResponseCode.SERVICE_NORMAL.getCode());

        return result;
    }

    /**
     * 返回API调用结果统一封装对象: success
     * @param message
     * @param object
     * @return
     */
    public static ResultVO resultSuccess(String message, Object object) {
        ResultVO result = new ResultVO();
        result.setTotal(0L);
        result.setMessage(message);
        result.setResult(object);
        result.setCode(ResponseCode.SERVICE_NORMAL.getCode());

        return result;
    }

    /**
     * 返回API调用结果统一封装对象: success
     * @param message
     * @param total
     * @param object
     * @return
     */
    public static ResultVO resultSuccess(String message, long total, Object object) {
        ResultVO result = new ResultVO();
        result.setTotal(0L);
        result.setMessage(message);
        result.setTotal(total);
        result.setResult(object);
        result.setCode(ResponseCode.SERVICE_NORMAL.getCode());

        return result;
    }

    /**
     * 返回API调用结果统一封装对象: error
     * @param message
     * @return
     */
    public static ResultVO resultError(String message) {
        ResultVO result = new ResultVO();
        result.setTotal(0L);
        result.setMessage(message);
        result.setCode(ResponseCode.SERVICE_INTERNAL_ERROR.getCode());

        return result;
    }

    /**
     * 返回API调用结果统一封装对象: error
     * @param message
     * @param code
     * @return
     */
    public static ResultVO resultError(String message, String code) {
        ResultVO result = new ResultVO();
        result.setTotal(0L);
        result.setMessage(message);
        result.setCode(code);

        return result;
    }

    /**
     * 生成权限应用的版本
     * @param code
     * @return
     */
    public static String generateApplicationVersion(String code) {
        return code + "#" + UUID();
    }

    /**
     * LocalDateTime 格式化为字符串
     * @param dateTime
     * @return
     */
    public static String formatDateTime(LocalDateTime dateTime) {
        if(null == dateTime) {
            return "";
        }
        return DATETIME_FORMATTER.format(dateTime);
    }

    /**
     * 格式化LocalDateTime字符串转换为LocalDateTime
     * @param dateTimeString
     * @return
     */
    public static LocalDateTime parseDateTime(String dateTimeString) {
        if(isEmpty(dateTimeString)) {
            return null;
        }
        return LocalDateTime.parse(dateTimeString, DATETIME_FORMATTER);
    }

    /**
     * LocalDate 格式化为字符串
     * @param date
     * @return
     */
    public static String formatDate(LocalDate date) {
        if(null == date) {
            return "";
        }
        return DATE_FORMATTER.format(date);
    }

    /**
     * 格式化LocalDate字符串转换为LocalDate
     * @param dateString
     * @return
     */
    public static LocalDate parseDate(String dateString) {
        if(isEmpty(dateString)) {
            return null;
        }
        return LocalDate.parse(dateString, DATE_FORMATTER);
    }

    /**
     * 生成mongodb模糊搜索的正则条件
     * @param searchText
     * @return
     */
    public static Pattern generateSearchPattern(String searchText) {
        return Pattern.compile("^.*" + searchText + ".*$", Pattern.CASE_INSENSITIVE);
    }

    /**
     * 获取本机ip地址
     * @return
     */
    public static InetAddress getLocalHostAddress() throws IOException {
        if(isEmpty(localhostIPAddress)) {
            localhostIPAddress = initLocalHostAddress();
        }
        return localhostIPAddress;
    }

    private static InetAddress initLocalHostAddress() throws IOException {
        // 优先池
        List<InetAddress> priorityAddressPool = new ArrayList<>();
        // 候选池: IPv4
        List<InetAddress> candidateAddressPool4Ipv4 = new ArrayList<>();
        // 候选池: IPv6
        List<InetAddress> candidateAddressPool4Ipv6 = new ArrayList<>();
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            // 网卡链接激活状态
            if(networkInterface.isUp()) {
                for (Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses(); inetAddresses.hasMoreElements(); ) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    // 排除 Loop back 回环类型地址或链路本地地址
                    if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()) {
                        if (inetAddress instanceof Inet4Address) {
                            if (inetAddress.isSiteLocalAddress() && !networkInterface.getName().contains(NETWORK_INTERFACE_NAME_DOCKER)) {
                                // 如果是site-local地址, 且并非docker虚拟地址，进入优先池
                                priorityAddressPool.add(inetAddress);
                            } else {
                                // 否则, 进入候选池
                                candidateAddressPool4Ipv4.add(inetAddress);
                            }
                        } else if (inetAddress instanceof Inet6Address) {
                            candidateAddressPool4Ipv6.add(inetAddress);
                        }
                    }
                }
            }
        }

        if(isNotEmpty(priorityAddressPool)) {
            return priorityAddressPool.get(0);
        }
        if(isNotEmpty(candidateAddressPool4Ipv4)) {
            return candidateAddressPool4Ipv4.get(0);
        }
        if(isNotEmpty(candidateAddressPool4Ipv6)) {
            return candidateAddressPool4Ipv6.get(0);
        }
        return InetAddress.getLocalHost();
    }

    /**
     * InetSocketAddress to string with port
     * @param address
     * @return
     */
    public static String generateAddressStringWithPort(InetSocketAddress address) {
        return String.format("%s:%d", address.getAddress().getHostAddress(), address.getPort());
    }

    /**
     * InetAddress to string
     * @param address
     * @return
     */
    public static String generateAddressString(InetAddress address) {
        return address.getHostAddress();
    }

    /**
     * InetSocketAddress to string without port
     * @param address
     * @return
     */
    public static String generateAddressString(InetSocketAddress address) {
        return address.getAddress().getHostAddress();
    }

    /**
     * get java method parameter name in class
     * @param method
     * @param index
     * @return
     */
    public static String getJavaMethodParameterName(Method method, Integer index) {
        return DISCOVERER.getParameterNames(method)[index];
    }

    /**
     * join collection
     * @param c
     * @param splitChar
     * @return
     */
    public static String joinCollection(Collection<?> c, String splitChar) {
        StringBuffer buffer = new StringBuffer();
        if(isNotEmpty(c)) {
            c.forEach(i -> {
                buffer.append(i).append(splitChar);
            });
            buffer.deleteCharAt(buffer.length() - 1);
        }
        return buffer.toString();
    }

}
