package io.gitee.declear.common.utils.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * API调用结果统一封装对象
 * @author dec
 */
@Data
@NoArgsConstructor
public class ResultVO {

    private String code;

    private String message;

    private Long total;

    private Object result;

}
