package io.gitee.declear.common.utils.enums;

/**
 * API调用结果 response code
 * @author dec
 */
public enum ResponseCode {

    /**
     * 服务处理时内部出错
     */
    SERVICE_INTERNAL_ERROR("DEC_CODE_RESPONSE_10002", "service_internal_error"),

    /**
     * 服务处理正常
     */
    SERVICE_NORMAL("DEC_CODE_RESPONSE_10020", "service_normal");

    private String code;

    private String value;

    ResponseCode(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
